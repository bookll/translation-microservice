# Translation Microservice #

Microservice to translate a piece of text, using the Google Translation API. 

http://sentiment-microservice.herokuapp.com

### What is this repository for? ###

* Pass text in to a URL via query string to receive a translation
* Version 1.0

### How do I get set up? ###

* Clone repo
* Install deps
* npm start

### Params ###

* text [String]
* target [ en / fr / de / es ] etc.

To see the languages that Google Translate supports go to https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes

### Example usage ###


```
#!js

axios.get('http://translation-microservice.herokuapp.com', {
    params: {
      text: "Bonjour la monde !",
      target: "en"

    }
  })
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error);
  });  
```

### Result ###


```
#!json

{
    "text": "Hello world!",
    "from": {
        "language": {
            "didYouMean": false,
            "iso": "fr"
        },
        "text": {
            "autoCorrected": false,
            "value": "",
            "didYouMean": false
        }
    },
    "raw": ""
}
```