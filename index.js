require('dotenv').config()

const { send } = require('micro')
const url = require('url')
const translate = require('google-translate-api');

module.exports = function(req, res) {

    const path = url.parse(req.url, true);
    const text = path.query.text || "Bonjour le monde !";
    const target = path.query.to || "en";

    translate(text, {to: target}).then(data => {

            const translation = JSON.stringify(data, null, 4);
            res.write(translation);
            send(res, 200)

    }).catch(err => {
        console.error(err);
    });    

}